<?php

use yii\db\Migration;

/**
 * Class m180622_063350_init_rbac
 */
class m180622_063350_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $auth = Yii::$app->authManager;
        //כתיבת כל ההרשאות
                // add "createUser" permission
                $createUser = $auth->createPermission('createUser');
                $createUser->description = 'Create a user';
                $auth->add($createUser);
        
                // add "updateUser" permission
                $updateUser = $auth->createPermission('updateUser');
                $updateUser->description = 'Update user';
                $auth->add($updateUser);
        
                // add "DeleteUser" permission
                $deleteUser = $auth->createPermission('deleteUser');
                $deleteUser->description = 'Delete a user';
                $auth->add($deleteUser);
        
                // add "createPost" permission
                $createPost = $auth->createPermission('createPost');
                $createPost->description = 'Create a post';
                $auth->add($createPost);
        
                // add "updatePost" permission
                $updatePost = $auth->createPermission('updatePost');
                $updatePost->description = 'Update post';
                $auth->add($updatePost);
        
                // add "updateOwnPost" permission
                $updateOwnPost = $auth->createPermission('updateOwnPost');
                $updateOwnPost->description = 'Update own post';
        
                $rule = new \app\rbac\AuthorRule;
                $auth->add($rule);
        
                $updateOwnPost->ruleName = $rule->name;
                $auth->add($updateOwnPost);
        
                // add "publishPost" permission
                $publishPost = $auth->createPermission('publishPost');
                $publishPost->description = 'Publish post';
        
                $auth->add($publishPost);
        
                // add "DeletePost" permission
                $deletePost = $auth->createPermission('deletePost');
                $deletePost->description = 'Delete a post';
                $auth->add($deletePost);
        
                // add "ViewPostList" permission
                $viewPostList = $auth->createPermission('viewPostList');
                $viewPostList->description = 'View post list';
                $auth->add($viewPostList);
        
                // add "ViewPublishPostList" permission
                $viewPublishPostList = $auth->createPermission('viewPublishPostList');
                $viewPublishPostList->description = 'View published post list';
                $auth->add($viewPublishPostList);
        //יצירת תפקידים וההרשאות של כל תפקיד
                // add "author" role and give this role permission
                $author = $auth->createRole('author');
                $auth->add($author);
                $auth->addChild($author, $createPost);
                $auth->addChild($author, $updateOwnPost);
                $auth->addChild($author, $viewPostList);        
        
                // add "editor" role and give this role permission
                $editor = $auth->createRole('editor');
                $auth->add($editor);
                $auth->addChild($editor, $updatePost);
                $auth->addChild($editor, $publishPost);
                $auth->addChild($editor, $deletePost);
                $auth->addChild($editor, $author);
        
                // add "admin" role and give this role permission
                $admin = $auth->createRole('admin');
                $auth->add($admin);
                $auth->addChild($admin, $createUser);
                $auth->addChild($admin, $updateUser);
                 $auth->addChild($admin, $deleteUser);
                $auth->addChild($admin, $editor);
        //לקשר יוזר לתפקידים
                // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
                // usually implemented in your User model.
                $auth->assign($editor, 2);
                $auth->assign($author, 7);
                $auth->assign($admin, 3);
            }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180622_063350_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_063350_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}







